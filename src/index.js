import React from 'react';
import ReactDOM from 'react-dom';
import * as serviceWorker from './serviceWorker';

import Layout from './containers/Layout/Layout';
import './containers/Layout/Layout.css';

ReactDOM.render(<Layout />, document.getElementById('root'));
serviceWorker.register();