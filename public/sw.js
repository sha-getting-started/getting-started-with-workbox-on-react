importScripts('https://storage.googleapis.com/workbox-cdn/releases/3.4.1/workbox-sw.js');

// Precache all static files.
workbox.routing.registerRoute('http://localhost:3000/', 
    workbox.strategies.networkFirst());
    
workbox.routing.registerRoute(/\.(?:js|css|html|png|jpeg|jpg)$/, 
    workbox.strategies.networkFirst());